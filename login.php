<?php
/*
 * login.php
 * 
 * Copyright 2014 Saurabh Srivastava <ssri@cse.iitk.ac.in>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	$password = "h8-hec-18";
	session_start();
	$supplied_password = $_POST["password"];
	$exit_code = -1;
	$redirect = null;
	if($_SERVER["SERVER_ADDR"] === $_SERVER["REMOTE_ADDR"]) {
		$_SESSION["admin"] = true;
		$exit_code = 0;
		$redirect = "admin.php";
	} else if($password === $supplied_password) {
		$oldmask = umask(0);
		$return = mkdir("./votes/".$_SERVER["REMOTE_ADDR"], 0770, true);
		umask($oldmask);
		/*if($return !== true) {
			$exit_code = 1;
			$redirect = "login.html";
		} else {
			$exit_code = 0;
			$redirect = "booth.php";
		}*/
		$redirect = "booth.php";
		$exit_code = 0;
	} else {
		$exit_code = 2;
		$redirect = "login.html";
	}
	$message = "";
	if($exit_code === 1)
		$message = "Can not write on Server. Please enable permissions and retry";
	else if($exit_code === 2)
		$message = "Invalid Password";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>Admin Login</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="generator" content="Geany 0.21" />
	<script type="text/javascript">
		<?php
		if($exit_code !== 0) {
		?>
			alert('<?=$message?>');
		<?php
		}
		?>
		window.location = "<?=$redirect?>";
	</script>
</head>

<body>
	
</body>

</html>

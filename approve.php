<?php
/*
 * approve.php
 * 
 * Copyright 2014 Saurabh Srivastava <ssri@cse.iitk.ac.in>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

	session_start();
	header('Content-type: application/xml');
	if($_SERVER["REMOTE_ADDR"] !== $_SERVER["SERVER_ADDR"])
		die('<?xml version="1.0" encoding="UTF-8"?><response><error code="1">The Admin Panel is accessible only on the Server</error></response>');
	if($_SESSION["admin"] !== true)	
		die('<?xml version="1.0" encoding="UTF-8"?><response><error code="2">Not logged in</error></response>');
	
	if(file_exists("./votes/votes.xml") === FALSE) {
		$dataDoc = simplexml_load_file("data.xml");
		$meta = $dataDoc->{"meta"};
		$nota = false;
		if(isset($meta->{"nota"}))
			$nota = filter_var($meta->{"nota"}, FILTER_VALIDATE_BOOLEAN);
		$postData = $dataDoc->{"post-data"};
		$dom = new DOMDocument('1.0');
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;
		$dom->loadXML($postData->asXML());
		$candidates = $dom->getElementsByTagName('candidate');
		foreach($candidates as $candidate) {
			$img = $candidate->getElementsByTagName('img')->item(0);
			$candidate->removeChild($img);
			$votes = $dom->createElement("votes", "0");
			$candidate->appendChild($votes);
		}
		if($nota === true) {
			$posts = $dom->getElementsByTagName('post');
			foreach($posts as $post) {
				$dummyCandidate = $dom->createElement("candidate");
				$id = $dom->createElement("id", "nota");
				$name = $dom->createElement("name", "None of the Above");
				$votes = $dom->createElement("votes", "0");
				$dummyCandidate->appendChild($id);
				$dummyCandidate->appendChild($name);
				$dummyCandidate->appendChild($votes);
				$post->appendChild($dummyCandidate);
			}
		}
		//print_r($candidates);
		$dom->save("./votes/votes.xml");
	}
	$dom = new DOMDocument('1.0');
	$dom->preserveWhiteSpace = false;
	$dom->formatOutput = true;
	$dom->loadXML(file_get_contents("./votes/votes.xml"));
	$booth = $_POST["booth"];
	$voter = $_POST["roll"];
	$lines = explode("\n", trim(file_get_contents("./votes/$booth/$voter")));
	$votes = array();
	foreach($lines as $line) {
		$pair = explode(":", $line);
		$votes[$pair[0]] = $pair[1];
	}
	$posts = $dom->getElementsByTagName('post');
	foreach($posts as $post) {
		$id = $post->getAttribute("id");
		if(isset($votes[$id])) {
			$vote = $votes[$id];
			$candidates = $post->getElementsByTagName('candidate');
			foreach($candidates as $candidate) {
				$cand_id = $candidate->getElementsByTagName('id')->item(0)->nodeValue;
				if($cand_id === $vote) {
					$curr_votes = intval($candidate->getElementsByTagName('votes')->item(0)->nodeValue);
					$curr_votes = $curr_votes+1;
					$candidate->getElementsByTagName('votes')->item(0)->nodeValue = "$curr_votes";
					break;
				}
			}
		}
	}
	// Add Log
	$log = file_get_contents("./votes/log");
	if($log === false)
		$log = "";
	$time = getdate();
	$str = $time["mday"]."-".$time["mon"]."-".$time["year"]." ".$time["hours"].":".$time["minutes"].":".$time["seconds"]." -- ";
	$str = "$str$voter($booth)";
	file_put_contents("./votes/log", "$log$str\n");
	// Add to Voted List
	$voted = trim(file_get_contents("./votes/voted.csv"));
	if(strlen($voted) > 0)
		$voted = "$voted,";
	file_put_contents("./votes/voted.csv", "$voted$voter");
	unlink("./votes/$booth/$voter");
	unlink("./votes/votes.xml");
	$dom->save("./votes/votes.xml");
	echo '<?xml version="1.0" encoding="UTF-8"?><response><approved>'.$voter.'</approved></response>';
?>

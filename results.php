<?php
/*
 * results.php
 * 
 * Copyright 2014 Saurabh Srivastava <ssri@cse.iitk.ac.in>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	session_start();
	if($_SERVER["REMOTE_ADDR"] !== $_SERVER["SERVER_ADDR"])
		die('<script type="text/javascript">alert("The Admin Panel is accessible only on the Server");</script>');
	if($_SESSION["admin"] !== true)
		header('Location: login.html');
	$dom = new DOMDocument('1.0');
	$dom->preserveWhiteSpace = false;
	$dom->formatOutput = true;
	$dom->loadXML(file_get_contents("./votes/votes.xml"));
	$posts = $dom->getElementsByTagName('post');
	$results = array();
	$winners = array();
	foreach($posts as $post) {
		$name = $post->getAttribute("name");
		$candidates = $post->getElementsByTagName('candidate');
		$post_results = array();
		foreach($candidates as $candidate) {
			$cand_name = $candidate->getElementsByTagName('name')->item(0)->nodeValue;
			$votes = intval($candidate->getElementsByTagName('votes')->item(0)->nodeValue);
			$post_results[$cand_name] = $votes;
		}
		$results[$name] = $post_results;
	}
	$dataDoc = simplexml_load_file("data.xml");
	$meta = $dataDoc->{"meta"};
	$banner = null;
	if(isset($meta->{"banner"}))
		$banner = $meta->{"banner"};
	$title = "Elections";
	if(isset($meta->{"title"}))
		$title = $meta->{"title"};
	//print_r($results);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>Voting Results</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="generator" content="Geany 0.21" />
	<link rel="stylesheet" type="text/css" href="css/results.css" />
	<script type="text/javascript">alert("WARNING: Do not press \"Refresh\" or \"Back\" button before taking a print-out of the results !!\nIf you wish to see Election logs, open it \"after\" you have printed the results.");</script>
</head>

<body>
	<div id="results-container">
		<div class="banner">
			<?php
			if($banner != null) {
			?>
				<img src="<?=$banner?>"/>
			<?php
			} else {
			?>
				<h1>Election Committee Desk</h1>
			<?php
			}
			?>
				<h1><?=$title?><h1>
		</div>
		<center><h2>Election Results</h2></center>
		<div id="result">
			<?php
			foreach($results as $post=>$post_results) {
				?>
				<div class="post">
					<span class="post-name"><?=$post?></span>
					<?php
					foreach($post_results as $cand=>$votes) {
						?>
						<div class="candidate">
						<span class="candidate-name"><?=$cand?></span>:&nbsp;&nbsp;<span class="votes"><?=$votes?></span> Votes
						</div>
						<?php
					}
					?>
				</div>
				<?php
			}
			$pwd = getcwd();
			mkdir("$pwd/bck", 0770, true);
			exec("mv $pwd/votes/* $pwd/bck");
			?>
		</div>
	</div>
	<center>
	<br/> <input type="button" value="Print Results" id="print" /> <br/>
	If you are not connected to a Printer right now, most browsers also provide an option to "print" to a PDF File.
	<br/><br/>
	<a href="bck/log" target="_blank">View Logs</a>
	</center>
</body>
<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="js/printThis.js"></script>
<script type="text/javascript" src="js/results.js"></script>
</html>

<?php
/*
 * booth.php
 * 
 * Copyright 2014 Saurabh Srivastava <ssri@cse.iitk.ac.in>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	$dataDoc = simplexml_load_file("data.xml");
	$meta = $dataDoc->{"meta"};
	$title = "Elections";
	if(isset($meta->{"title"}))
		$title = $meta->{"title"};
	$banner = null;
	if(isset($meta->{"banner"}))
		$banner = $meta->{"banner"};
	$nota = false;
	if(isset($meta->{"nota"}))
		$nota = filter_var($meta->{"nota"}, FILTER_VALIDATE_BOOLEAN);
	$notaImg = "images/nota.jpeg";
	if(isset($meta->{"nota-image"}))
		$notaImg = $meta->{"nota-image"};
	$imgHeight = 160;
	if(isset($meta->{"cand-img-height"}))
		$imgHeight = intval($meta->{"cand-img-height"});
	//echo("$title\n$banner\n$nota\n$imgHeight\n$imgWidth");
	$postData = $dataDoc->{"post-data"};
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title><?=$title?></title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="generator" content="Geany 0.21" />
	<link rel="stylesheet" type="text/css" href="css/booth.css" />
</head>

<body>
	<div class="banner">
		<?php
		if($banner != null) {
		?>
			<img src="<?=$banner?>"/>
		<?php
		} else {
		?>
			<h1>Election</h1>
		<?php
		}
		?>
	<h1><?=$title?><h1>
	</div>
	<form method="post" action="vote.php">
		<div class="rollno">
			Enter Your Roll Number &nbsp;<input type="text" id="rollno" name="rollno" />
		</div>
		<?php
		//print_r($postData);
		foreach($postData->{"post"} as $post) {
			$post_name = $post["name"];
			$post_id = $post["id"];
			$candidates = $post->{"candidate"};
			?>
				<div class="post" style="height:<?=($imgHeight+75)?>px" id="<?=$post_id?>">
					<span class="post-name">Post: <b><?=$post_name?></b></span>
					<div class="candidates">
					<?php
					foreach($candidates as $candidate) {
						$candidate_name = $candidate->{"name"};
						$candidate_id = $candidate->{"id"};
						$candidate_img = $candidate->{"img"}
						?>
						<div class="candidate">
							<img height="<?=$imgHeight?>" src="<?=$candidate_img?>" /> <br />
							<input type="radio" name="<?=$post_id?>" value="<?=$candidate_id?>">
							<span class="candidate-name"><?=$candidate_name?></span>
						</div>
						<?php
					}
					if($nota) {
						?>
						<div class="candidate">
							<img src="<?=$notaImg?>" /> <br />
							<input type="radio" name="<?=$post_id?>" value="nota" checked="checked">
							<span class="candidate-name">None of the Above</span>
						</div>
						<?php
					}
					?>
					</div>
				</div>
			<?php
		}
		?>
		<input class="vote" type="submit" value="Vote" />
	</form>
</body>
<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="js/booth.js"></script>
</html>

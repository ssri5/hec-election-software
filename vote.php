<?php
/*
 * vote.php
 * 
 * Copyright 2014 Saurabh Srivastava <ssri@cse.iitk.ac.in>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	$exit_code = 0;
	$title = "Thanks for Voting";
	$message = "Voting Successful";
	$redirect = "booth.php";
	//print_r($_REQUEST);
	//print_r($_SERVER["REMOTE_ADDR"]);
	$polling_station = $_SERVER["REMOTE_ADDR"];
	$listing = scandir("./votes/");
	if(array_search($polling_station, $listing) !== FALSE) {
		$rollNo = trim($_POST["rollno"]);
		$allowedRollNos = explode(",", trim(file_get_contents("voters.csv")));
		if(array_search($rollNo, $allowedRollNos) !== FALSE) {
			$alreadyVoted = explode(",", trim(file_get_contents("votes/voted.csv")));
			if(array_search($rollNo, $alreadyVoted) === FALSE) {
				// Prepare Vote Content:
				unset($_POST["rollno"]);
				$content = "";
				foreach($_POST as $key => $value) {
					$content = "$content$key:$value\n";
				}
				file_put_contents("./votes/$polling_station/$rollNo", $content);
			} else {
				$exit_code = 3;
			}
		} else {
			$exit_code = 2;
		}
	} else {
		$exit_code = 1;
	}
	
	if($exit_code !== 0) {
		$title = "Vote not registered";
		$redirect = "javascript: history.back()";
		switch($exit_code) {
			case 1:
				$message = "Invalid Polling Booth";
				break;
			case 2:
				$message = "You are not allowed to vote in this Election";
				break;
			case 3:
				$message = "You have already voted in this Election";
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title><?=$title?></title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="generator" content="Geany 0.21" />
	<script type="text/javascript">
		alert('<?=$message?>');
		window.location = "<?=$redirect?>";
	</script>
</head>

<body>
	
</body>

</html>

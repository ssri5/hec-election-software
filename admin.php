<?php
/*
 * admin.php
 * 
 * Copyright 2014 Saurabh Srivastava <ssri@cse.iitk.ac.in>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	session_start();
	if($_SERVER["REMOTE_ADDR"] !== $_SERVER["SERVER_ADDR"])
		die('<script type="text/javascript">alert("The Admin Panel is accessible only on the Server");</script>');
	if($_SESSION["admin"] !== true)
		header('Location: login.html');
	$dataDoc = simplexml_load_file("data.xml");
	$meta = $dataDoc->{"meta"};
	$banner = null;
	if(isset($meta->{"banner"}))
		$banner = $meta->{"banner"};
	$title = "";
	if(isset($meta->{"title"}))
		$title = $meta->{"title"};
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>Election Committee Desk</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="generator" content="Geany 0.21" />
	<link rel="stylesheet" type="text/css" href="css/admin.css" />
</head>

<body>
	<div class="banner">
                <?php
                if($banner != null) {
                ?>
                        <img src="<?=$banner?>"/>
                <?php
                } else {
                ?>
                        <h1>Election Committee Desk</h1>
                <?php
                }
                ?>
                        <h1><?=$title?><h1>
                <?php
                ?>
    </div>
	<div class="admin-panel">
	</div>
	<center><a href="javascript:void(0)" id="end-poll">End Poll</a></center>
	<div id="overlay">
		<img src="images/InProgress.gif" />
	</div>
</body>
<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="js/admin.js"></script>
</html>

<?php
/*
 * unapproved.php
 * 
 * Copyright 2014 Saurabh Srivastava <ssri@cse.iitk.ac.in>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
	session_start();
	header('Content-type: application/xml');
	if($_SERVER["REMOTE_ADDR"] !== $_SERVER["SERVER_ADDR"])
		die('<?xml version="1.0" encoding="UTF-8"?><response><error code="1">The Admin Panel is accessible only on the Server</error></response>');
	if($_SESSION["admin"] !== true)	
		die('<?xml version="1.0" encoding="UTF-8"?><response><error code="2">Not logged in</error></response>');	
	$response = "";
	$path = "./votes/";
	$booths = array();
	$results = scandir($path);
	foreach ($results as $result) {
		if ($result === '.' or $result === '..' or is_dir("$path/$result") !== true)
			continue;
		array_push($booths, trim($result));
	}
	
	foreach($booths as $booth) {
		$files = scandir("$path/$booth");
		$boothResponse = "";
		foreach($files as $file) {
			if($file === '.' or $file === '..' or is_file("$path/$booth/$file") !== true)
				continue;
			$boothResponse = "$boothResponse<voter>$file</voter>";
		}
		$response = "$response<booth ip='$booth'>$boothResponse</booth>";
	}
	$response = "<?xml version='1.0' encoding='UTF-8'?><response>$response</response>";
	echo $response;
?>

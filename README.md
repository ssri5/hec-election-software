# README #

This repository has the source code for Hall Election Software being used at Hall 8, IIT Kanpur

### How do I get set up? ###

0. Download/Copy the contents at some temporary location, say a folder called "election" in /tmp

1. Install PHP, Apache2. Use the command:  
sudo apt install php7.4 apache2 libapache2-mod-php7.4

2. Install the XML support and restart the apache server  
sudo apt install php7.4-xml  
sudo service apache2 restart  

3. Create a folder called "hec" in the /var/www/html folder  
cd /var/www/html  
sudo mkdir election  

4. Copy the contents inside the hec folder  
sudo cp -r /tmp/hec/* /var/www/html/election/  

5. Change Permissions and owner of the hec folder:  
cd /var/www/html  
sudo chown -R $USER:www-data election  
sudo chmod -R 774 election

6. Open the data.xml file, and read the instructions to add the required information. You will need to provide names and photos of the candidates for each post.

7. Copy and paste the voter's Roll Numbers in the files voters.csv

/*
 * booth.js
 * 
 * Copyright 2014 Saurabh Srivastava <ssri@cse.iitk.ac.in>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

$(document).ready(
	function() {
		$("form").submit(
			function() {
				roll = $("#rollno").val().trim();
				if(roll.length == 0) {
					alert('Please Enter your Roll Number');
					$("#rollno").focus();
					$("#rollno").parent().css("border", "2px red solid");
					$("#rollno").parent().css("background-color", "lightcyan");
					return false;
				}
				
				choice = confirm("Are you sure you want to submit your vote? It can't be changed once submitted.");
				if(choice == false)
					return false;
				$("#rollno").val(roll.toUpperCase());
			}
		);
	}
);

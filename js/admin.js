/*
 * admin.js
 * 
 * Copyright 2014 Saurabh Srivastava <ssri@cse.iitk.ac.in>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

$(document).ready(
	function() {
		var height= $(window).height();
		var loader_height = $("#overlay img").outerHeight();
		//$("#overlay").fadeTo(0.7);
		//$("#overlay img").fadeTo(1.0);
		$("#overlay img").css("margin-top", ((height-loader_height)/2) + "px");
		$("#end-poll").click(
			function() {
				if(confirm("Are you sure you want to end the poll?") == true) {
					window.location = "results.php";
				}
			}
		);
		refresh();
	}
);
var timer = null;
function refresh() {
	
	$.ajax({
		url: "unapproved.php",
		dataType: "xml",
		type: "POST",
		success:
			function(result) {
				//console.log(result);
				$xml = $(result);
				$response = $("response",$xml);
				if($("error", $response).length > 0) {
					$error = $("error", $response);
					alert($error.text());
					if($error.attr("code") == "2")
						window.location = "login.html";
					return;
				}
				//console.log($response);
				timer = setTimeout(refresh, 1000);
				$("div.admin-panel").empty();
				$($response).children().each(
					function() {
						//console.log($(this));
						boothIP = $(this).attr("ip").trim();
						$boothDiv = $('<div class="polling-booth" id="booth_' + boothIP + '"></div>');
						$boothDiv.append('<span class="polling-booth-ip">' + boothIP + '</span>');
						$boothDiv.append('<hr/>');
						$(this).children().each(
							function() {
								voter = $(this).text().trim();
								$voteDiv = $('<div class="vote" id="voter_' + voter + '"></div>');
								$voteDiv.append('<span class="rollno">' + voter + '</span>&nbsp;');
								$voteDiv.append('<input class="approve" type="button" value="Approve" />');
								$voteDiv.append('<input class="deny" type="button" value="Deny" />');
								$("input.approve", $voteDiv).click(
									function(event) {
										$button = $(event.target);
										voterRoll = $button.parent().attr("id").split("_")[1];
										boothIP = $button.parent().parent().attr("id").split("_")[1];
										approve(boothIP, voterRoll);
									}
								);
								$("input.deny", $voteDiv).click(
									function(event) {
										$button = $(event.target);
										voterRoll = $button.parent().attr("id").split("_")[1];
										boothIP = $button.parent().parent().attr("id").split("_")[1];
										deny(boothIP, voterRoll);
									}
								);
								$boothDiv.append($voteDiv);
							}
						);
						$("div.admin-panel").append($boothDiv);
					}
				);
			},
		error:
			function(e1, e2, e3) {
				alert("Could not Complete Call.\n" + e1 + " | " + e2 + " | " + e3);
			}
	});
}

function approve(booth, voter) {
	//clearTimeout(timer);
	$("#overlay").css("display", "block");
	$.ajax({
		url: "approve.php",
		dataType: "xml",
		type: "POST",
		data: {"booth" : booth, "roll" : voter},
		success: 
			function(result) {
				$xml = $(result);
				$response = $("response",$xml);
				if($("error", $response).length > 0) {
					$error = $("error", $response);
					alert($error.text());
					if($error.attr("code") == "2")
						window.location = "login.html";
					return;
				}
				id = "voter_" + voter;
				$("div#"+id).remove();
				$("#overlay").css("display", "none");
			},
		error:
			function(e1, e2, e3) {
				alert("Could not Complete Call.\n" + e1 + " | " + e2 + " | " + e3);
				console.log("Could not Complete Call.\n" + e1 + " | " + e2 + " | " + e3);
				$("#overlay").css("display", "none");
			}
	});
}

function deny(booth, voter) {
	//clearTimeout(timer);
	$("#overlay").css("display", "block");
	$.ajax({
		url: "deny.php",
		dataType: "xml",
		type: "POST",
		data: {"booth" : booth, "roll" : voter},
		success: 
			function(result) {
				$xml = $(result);
				console.log(result);
				$response = $("response",$xml);
				if($("error", $response).length > 0) {
					$error = $("error", $response);
					alert($error.text());
					if($error.attr("code") == "2")
						window.location = "login.html";
					return;
				}
				id = "voter_" + voter;
				$("div#"+id).remove();
				$("#overlay").css("display", "none");
				console.log("here");
			},
		error:
			function(e1, e2, e3) {
				alert("Could not Complete Call.\n" + e1 + " | " + e2 + " | " + e3);
				console.log("Could not Complete Call.\n" + e1 + " | " + e2 + " | " + e3);
				$("#overlay").css("display", "none");
			}
	});
}
